from urllib.parse import urlparse
import requests
from bs4 import BeautifulSoup
import string
import os

def check_url(url):
    parsed_url = urlparse(url)
    if all([parsed_url.scheme, parsed_url.netloc]):
        return True
    return False


class InternetMachine:

    def __init__(self):
        self.max_pages = int(input())
        self.article_type = input()
        self.main_url = "https://www.nature.com/nature/articles?sort=PubDate&year=2020"
        parsed_url = urlparse(self.main_url)
        self.base_url = f"{parsed_url.scheme}://{parsed_url.netloc}"

    def talk(self):
        current_page = self.main_url
        for page in range(1, self.max_pages+1):
            page_soup = self._process_page(current_page, page)
            next_page = page_soup.findAll("li", {"class": "c-pagination__item", "data-page": str(page+1)})
            current_page = self.base_url + next_page[0].find("a")["href"]

    def _process_page(self, url,  page_number):
        response = requests.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')
        articles = soup.findAll('article')
        page_folder = f"Page_{page_number}"
        os.makedirs(page_folder, exist_ok=True)
        for article in articles:
            span_type = article.find("span", {"class":"c-meta__type"})
            if span_type.text == self.article_type:
                new_article_path = article.find("a", {"class": "c-card__link"})["href"]
                new_url = f"{self.base_url}{new_article_path}"
                self._process_article(new_url, page_folder)
        return soup

    def _process_article(self, url, folder):
        response = requests.get(url)
        soup = BeautifulSoup(response.content, "html.parser")
        title = soup.find("title").text.strip()
        body = soup.find("div", {"class": "c-article-body u-clearfix"}).text.strip()
        file_name_slug = title.translate(str.maketrans('', '', string.punctuation)).replace(" ", "_")
        file_name = f"{file_name_slug}.txt"
        with open(f"{folder}/{file_name}", "wb") as output_file:
            output_file.write(body.encode("utf-8"))


InternetMachine().talk()
